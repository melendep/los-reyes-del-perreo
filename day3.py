# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 16:49:10 2022

@author: Luis Escobar y Pablo melendez
"""

"""
 Copyright (c) 2020 Alan Yorinks All rights reserved.
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
 Version 3 as published by the Free Software Foundation; either
 or (at your option) any later version.
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
 along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

import time
import sys
from pymata4 import pymata4

"""
Setup a pin for digital input and monitor its changes
Both polling and callback are being used in this example.
"""


# Setup a pin for analog input and monitor its changes


DIGITAL_PIN1= 14  # arduino pin number
DIGITAL_PIN2= 15
DIGITAL_PIN3= 16
DIGITAL_PIN4= 17

DIGITAL_PIN5 = 18  # arduino pin number
DIGITAL_PIN6= 19
DIGITAL_PIN7= 20
DIGITAL_PIN8= 21

POLL_TIME = 0.2  # number of seconds between polls

# Callback data indices
# Callback data indices
CB_PIN_MODE = 0
CB_PIN = 1
CB_VALUE = 2
CB_TIME = 3

digicode = {(14,18):'1', (14,19):'4', (14,20):'7', (14,21):'*',
            (15,18):'2', (15,19):'5', (15,20):'8', (15,21):'0',
            (16,18):'3', (16,19):'6', (16,20):'9', (16,21):'#',
            (17,18):'A', (17,19):'B', (17,20):'C', (17,21):'D'}

def the_callback(data):
    """
    A callback function to report data changes.
    This will print the pin number, its reported value and
    the date and time when the change occurred
    :param data: [pin, current reported value, pin_mode, timestamp]
    """
    date = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(data[CB_TIME]))
    print(f'Pin: {data[CB_PIN]} Value: {data[CB_VALUE]} Time Stamp: {date}')


def digital_in(my_board, pin):
    """
     This function establishes the pin as a
     digital input. Any changes on this pin will
     be reported through the call back function.
     :param my_board: a pymata4 instance
     :param pin: Arduino pin number
     """

    my_board.set_pin_mode_digital_input(pin)#, callback=the_callback)

    
def blink(my_board, pin):
    """
    This function will to toggle a digital pin.
    :param my_board: an PymataExpress instance
    :param pin: pin to be controlled
    """
    # set the pin mode
    my_board.set_pin_mode_digital_output(pin)
    time.sleep(0.01)
    my_board.digital_write(pin,0)


def config1():
    digital_in(board, DIGITAL_PIN1)
    digital_in(board, DIGITAL_PIN2)
    digital_in(board, DIGITAL_PIN3)
    digital_in(board, DIGITAL_PIN4)
    blink(board, DIGITAL_PIN5)
    blink(board, DIGITAL_PIN6)
    blink(board, DIGITAL_PIN7)
    blink(board, DIGITAL_PIN8)
    
def config2():
    digital_in(board, DIGITAL_PIN5)
    digital_in(board, DIGITAL_PIN6)
    digital_in(board, DIGITAL_PIN7)
    digital_in(board, DIGITAL_PIN8)
    blink(board, DIGITAL_PIN1)
    blink(board, DIGITAL_PIN2)
    blink(board, DIGITAL_PIN3)
    blink(board, DIGITAL_PIN4)
    


    
def second(pin,buffer):
    

    while True:

        try:
            
            value1, time_stamp1 = board.digital_read(DIGITAL_PIN5)
            #time.sleep(0.05)
            value2, time_stamp2 = board.digital_read(DIGITAL_PIN6)
            #time.sleep(0.05)
            value3, time_stamp3 = board.digital_read(DIGITAL_PIN7)
            #time.sleep(0.05)
            value4, time_stamp4 = board.digital_read(DIGITAL_PIN8)
            #time.sleep(0.1)

            if value1==0:
                print('Pin:', DIGITAL_PIN5)
                pin.append(DIGITAL_PIN5)
                buffer.append(digicode[tuple(pin)])
                time1=time.time()
                
                print(pin,buffer)
                while(checker(DIGITAL_PIN5)==0):
                    time1=time.time()
                    pass
                config1()
                time.sleep(0.05)
                
                return time1
                #break
            elif value2==0:
                print('Pin:', DIGITAL_PIN6)
                pin.append(DIGITAL_PIN6)
                buffer.append(digicode[tuple(pin)])
                time1=time.time()
                               
                print(pin,buffer)
                while(checker(DIGITAL_PIN6)==0):
                    time1=time.time()
                    pass
                config1()
                time.sleep(0.05)
                return time1
            elif value3==0:
                print('Pin:', DIGITAL_PIN7)
                pin.append(DIGITAL_PIN7)
                buffer.append(digicode[tuple(pin)])
                time1=time.time()
                
                print(pin,buffer)
                while(checker(DIGITAL_PIN7)==0):
                    time1=time.time()
                    pass
                config1()
                time.sleep(0.05)
                return time1
            elif value4==0:
                print('Pin:', DIGITAL_PIN8)
                pin.append(DIGITAL_PIN8)
                buffer.append(digicode[tuple(pin)])
                time1=time.time()
                
                print(pin,buffer)
                while(checker(DIGITAL_PIN8)==0):
                    time1=time.time()
                    pass
                config1()
                time.sleep(0.05)
                return time1
        except KeyboardInterrupt:
            board.shutdown()
            sys.exit(0)
            
def checker(DIGITAL_PIN):
    value, time_stamp = board.digital_read(DIGITAL_PIN)   
    #time1=time.time()
    return value
    
def first(DIGITAL_PIN,buffer):
    value, time_stamp = board.digital_read(DIGITAL_PIN)
    date = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time_stamp))
    time.sleep(0.01)
    pin=[]
    if (value==0) and (date.split('-')[0]=='2022'):
        print(value,date)
        print('Pin:', DIGITAL_PIN)
        pin.append(DIGITAL_PIN)
        
        #reconfiguration of column and rows
        config2()
        time1=second(pin,buffer)
        return time1
        

board = pymata4.Pymata4()

try:
    config1()
    buffer=[]
    time1=time.time()

    x=1
    while x==True:
        try:
            if (type(time1) == float ):
                time2=time.time()-time1
                if (time2)>5:
                    buffer=[]
                    print('buffer init')

                
                if (len(buffer)>=3 and buffer==['3','3','9']):
                    print('right code')
                    x=0
                elif(len(buffer)>=3 and buffer !=['3','3','9']):
                    print('wrong code')
                    print('buffer init')
                    buffer=[]
                
            time1_=first(DIGITAL_PIN1,buffer)
            time2_=first(DIGITAL_PIN2,buffer)
            time3_=first(DIGITAL_PIN3,buffer)
            time4_=first(DIGITAL_PIN4,buffer)
            
            time_=[time1_,time2_,time3_,time4_]
            for time__ in time_:
                if (type(time__) == float ):
                    time1=time__
            #first(DIGITAL_PIN2,buffer)
            #first(DIGITAL_PIN3,buffer)
            #first(DIGITAL_PIN4,buffer)
          
        except KeyboardInterrupt:
            board.shutdown()
            sys.exit(0)
            
    board.shutdown()
    sys.exit(0)
            
except KeyboardInterrupt:
    board.shutdown()
    sys.exit(0)

    

    
    
